package org.nttdata.strutsstarter;

import junit.framework.TestCase;

import com.opensymphony.xwork2.Action;
import org.nttdata.strutsstarter.actions.HelloWorldAction;

public class HelloWorldActionTest extends TestCase {

	public void testHelloWorldAction() throws Exception {
		HelloWorldAction action = new HelloWorldAction();
		String result = action.execute();
		assertEquals(Action.SUCCESS, result);
	}
}
