package org.nttdata.strutsstarter.services;

import org.nttdata.strutsstarter.common.services.CrudGenericService;
import org.nttdata.strutsstarter.models.Owner;

import java.util.Set;

public interface OwnerService extends CrudGenericService<Owner, Integer> {
	Set<Integer> listId();
}
