package org.nttdata.strutsstarter.services.adapters;

import org.nttdata.strutsstarter.models.Owner;
import org.nttdata.strutsstarter.persistence.entites.OwnerEntity;
import org.nttdata.strutsstarter.persistence.repository.OwnerRepository;
import org.nttdata.strutsstarter.services.OwnerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@Transactional
public class OwnerServiceAdapter implements OwnerService {

	private final OwnerRepository ownerRepository;

	public OwnerServiceAdapter(OwnerRepository ownerRepository) {
		this.ownerRepository = ownerRepository;
	}

	@Override
	public void create(Owner owner) {
		ownerRepository.save(
			OwnerEntity.builder()
				.name(owner.getName())
				.build()
		);
	}

	@Override
	public List<Owner> listObject() {
		return ownerRepository.findAll().stream()
			.map(o -> Owner.builder()
				.id(o.getId())
				.name(o.getName())
				.build())
			.toList();
	}

	@Override
	public Owner singleObject(Integer id) {
		return ownerRepository.findById(id)
			.map(o -> Owner.builder()
				.id(o.getId())
				.name(o.getName())
				.build())
			.orElseThrow();
	}

	@Override
	public void update(Owner owner, Integer id) {

	}

	@Override
	public void delete(Integer id) {

	}

	@Override
	public Set<Integer> listId() {
		return ownerRepository.listOnlyId();
	}
}
