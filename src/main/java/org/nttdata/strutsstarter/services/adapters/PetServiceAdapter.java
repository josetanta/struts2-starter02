package org.nttdata.strutsstarter.services.adapters;

import org.nttdata.strutsstarter.models.Owner;
import org.nttdata.strutsstarter.models.Pet;
import org.nttdata.strutsstarter.persistence.entites.OwnerEntity;
import org.nttdata.strutsstarter.persistence.entites.PetEntity;
import org.nttdata.strutsstarter.persistence.repository.PetRepository;
import org.nttdata.strutsstarter.services.PetService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("petService")
@Transactional
public class PetServiceAdapter implements PetService {

	private final PetRepository petRepository;

	public PetServiceAdapter(PetRepository petRepository) {
		this.petRepository = petRepository;
	}

	@Override
	public void create(Pet object) {
		petRepository.save(
			PetEntity.builder()
				.name(object.getName())
				.petCode(object.getCodePet())
				.owner(OwnerEntity.builder().id(object.getOwner().getId()).build())
				.build()
		);
	}

	@Override
	public List<Pet> listObject() {
		return petRepository.findAll().stream()
			.map(o -> Pet.builder()
				.id(o.getId())
				.name(o.getName())
				.codePet(o.getPetCode())
				.owner(Owner.builder()
					.id(o.getOwner().getId())
					.name(o.getOwner().getName())
					.build())
				.build())
			.toList();
	}

	@Override
	public Pet singleObject(Integer id) {
		return petRepository.findById(id)
			.map(o -> Pet.builder()
				.id(o.getId())
				.name(o.getName())
				.codePet(o.getPetCode())
				.build())
			.orElseThrow();
	}

	@Override
	public void update(Pet object, Integer id) {
		var pet = single(id);
		
		pet.setName(object.getName());
		pet.setPetCode(object.getCodePet());
		
		petRepository.save(pet);
	}

	@Override
	public void delete(Integer id) {
		var pet = single(id);
		petRepository.delete(pet);
	}
	
	private PetEntity single(Integer id) {
		return petRepository.findById(id)
			.orElseThrow();
	}
}
