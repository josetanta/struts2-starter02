package org.nttdata.strutsstarter.services;

import org.nttdata.strutsstarter.common.services.CrudGenericService;
import org.nttdata.strutsstarter.models.Pet;

public interface PetService extends CrudGenericService<Pet, Integer> {
}
