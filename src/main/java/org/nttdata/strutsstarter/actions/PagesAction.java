package org.nttdata.strutsstarter.actions;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;

@Namespace("/pages")
public class PagesAction extends ActionSupport {

	@Action("page01")
	public String page01() {
		System.out.println("-------- Page 01 -------");
		return SUCCESS;
	}

	@Action("page02")
	public String page02() {
		System.out.println("-------- Page 02 -------");
		return SUCCESS;
	}

	@Action("page03")
	public String page03() {
		System.out.println("-------- Page 03 -------");
		return SUCCESS;
	}
}
