package org.nttdata.strutsstarter.actions;

import com.opensymphony.xwork2.ActionSupport;
import lombok.Getter;
import lombok.Setter;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.nttdata.strutsstarter.models.Owner;
import org.nttdata.strutsstarter.models.Pet;
import org.nttdata.strutsstarter.services.OwnerService;
import org.nttdata.strutsstarter.services.PetService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Namespace("/pets")
@ParentPackage("default")
public class PetAction extends ActionSupport {

	@java.io.Serial
	private static final long serialVersionUID = 1L;
	
	@Getter
	@Setter
	private List<Pet> petList;

	@Getter
	@Setter
	private Pet pet;

	@Getter
	@Setter
	private Integer petId;

	@Getter
	@Setter
	private Set<Integer> listOwnerId;

	@Getter
	@Setter
	private Integer selectedOwnerID;

	@Autowired
	private PetService petService;

	@Autowired
	private OwnerService ownerService;

	@Getter
	@Setter
	private String exportTo = "none";

	@Action(value = "allPets", results = {
		@Result(name = "success", location = "allPets.jsp"),
		@Result(name = "successPdf", location = "org.nttdata.strutsstarter.converters.pdf.views.ListPDFView", type = "pdfResult"),
		@Result(name = "successJson", params = {"excludeProperties", "listOwnerId,petId,exportTo,pet,selectedOwnerID", "add", "my-value"}, type = "json")
	})
	public String allPets() {
		setPetList(petService.listObject());
		setListOwnerId(ownerService.listId());

		if (exportTo.equals("pdf"))
			return "successPdf";
		
		else if (exportTo.equals("json"))
			return "successJson";

		return SUCCESS;
	}

	@Action(value = "registerPet", results = {
		@Result(name = "input", location = "allPets.jsp"),
		@Result(type = "redirectAction", location = "allPets")
	})
	public String registerPet() {
		var pet = getPet();
		pet.setOwner(Owner.builder()
			.id(getSelectedOwnerID())
			.build());
		petService.create(pet);
		return SUCCESS;
	}
	
	@Action("detailPet")
	public String detailPet() {
		if (Objects.isNull(petId)) {
			return ERROR;
		}
		setPet(petService.singleObject(petId));
		return SUCCESS;
	}

	@Action(value = "deletePet", results = {
		@Result(name = "success", location = "detailPet.jsp"),
		@Result(type = "redirectAction", location = "allPets"),
	})
	public String deletePet() {
		petService.delete(petId);
		return SUCCESS;
	}
	
	@Action(value = "updatePet", results = {
		@Result(name = "success", location = "detailPet.jsp"),
		@Result(type = "redirectAction", location = "detailPet", params = {"petId", "${petId}"}),
	})
	public String updatePet() {
		petService.update(pet, petId);
		setPet(petService.singleObject(petId));
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (Objects.isNull(getPet())) {
			return;
		}
		setListOwnerId(ownerService.listId());
		if (getPet().getName().isEmpty()) {
			addFieldError("pet.name", getText("pet.form.name.error"));
		}
		if (getPet().getCodePet().isEmpty()) {
			addFieldError("pet.codePet", getText("pet.form.code-pet.error"));
		}
	}

}
