package org.nttdata.strutsstarter.actions;

import com.opensymphony.xwork2.ActionSupport;
import lombok.Getter;
import lombok.Setter;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.nttdata.strutsstarter.models.Owner;
import org.nttdata.strutsstarter.services.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;

@Namespace("/owners")
public class OwnerAction extends ActionSupport {

	@java.io.Serial
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private Owner ownerBean;

	@Getter
	@Setter
	private List<Owner> ownerList;

	@Autowired
	private OwnerService ownerService;

	@Action(value = "ownerList", results = @Result(location = "registerOwner.jsp"))
	public String ownerList() {
		setOwnerList(ownerService.listObject());
		return SUCCESS;
	}

	@Action(value = "registerOwner", results = {
		@Result(name = "input", location = "registerOwner.jsp"),
		@Result(type = "redirectAction", location = "ownerList")
	})
	public String registerOwner() {
		ownerService.create(getOwnerBean());
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (Objects.isNull(getOwnerBean())) {
			return;
		}
		if (ownerBean.getName().isEmpty()) {
			addFieldError("ownerBean.name", getText("owner.form.name.error"));
		}
	}
}
