package org.nttdata.strutsstarter.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Pet {
	private Integer id;
	private String name;
	private String codePet;

	private Owner owner;
}
