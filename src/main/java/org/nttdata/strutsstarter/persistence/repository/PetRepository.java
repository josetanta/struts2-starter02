package org.nttdata.strutsstarter.persistence.repository;

import org.nttdata.strutsstarter.persistence.entites.PetEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PetRepository extends JpaRepository<PetEntity, Integer> {

	@Query("""
			SELECT p.id, p.name, p.petCode, ow.name FROM PetEntity p
			JOIN p.owner ow
		""")
	List<PetEntity> listPet();
}
