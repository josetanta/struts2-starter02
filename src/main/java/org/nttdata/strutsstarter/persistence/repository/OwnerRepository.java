package org.nttdata.strutsstarter.persistence.repository;

import org.nttdata.strutsstarter.persistence.entites.OwnerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface OwnerRepository extends JpaRepository<OwnerEntity, Integer> {

	@Query("select ow.id from OwnerEntity ow")
	Set<Integer> listOnlyId();
}
