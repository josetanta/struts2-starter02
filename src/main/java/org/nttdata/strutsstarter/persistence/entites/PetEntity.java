package org.nttdata.strutsstarter.persistence.entites;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "pets")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PetEntity {
	@Id
	@GeneratedValue
	private Integer id;
	private String name;

	@Column(unique = true)
	private String petCode;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id")
	private OwnerEntity owner;
}
