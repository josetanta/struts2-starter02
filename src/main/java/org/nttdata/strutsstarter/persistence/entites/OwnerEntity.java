package org.nttdata.strutsstarter.persistence.entites;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "owner")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OwnerEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Integer id;
	private String name;
}
