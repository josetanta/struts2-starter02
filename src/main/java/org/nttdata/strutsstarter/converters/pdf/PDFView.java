package org.nttdata.strutsstarter.converters.pdf;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionInvocation;

public abstract class PDFView {
	protected abstract void buildPdfDocument(
			ActionInvocation actionInvocation,
			Document document,
			PdfWriter writer,
			HttpServletRequest request,
			HttpServletResponse response
	) throws Exception;
}
