package org.nttdata.strutsstarter.converters.pdf.views;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nttdata.strutsstarter.actions.PetAction;
import org.nttdata.strutsstarter.converters.pdf.PDFView;
import org.nttdata.strutsstarter.models.Pet;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionInvocation;

public class ListPDFView extends PDFView {

	@Override
	protected void buildPdfDocument(ActionInvocation actionInvocation, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		PetAction petAction = (PetAction) actionInvocation.getAction();
		List<Pet> pets = petAction.getPetList();
		PdfPTable table = new PdfPTable(2);
		table.addCell("NamePet");
		table.addCell("CodePet");

		if (!pets.isEmpty())
			for (Pet pet : pets) {
				table.addCell(pet.getName());
				table.addCell(pet.getCodePet());
			}

		document.add(table);
		document.add(new Paragraph("@lookafter_your_pets 😀"));
	}
}
