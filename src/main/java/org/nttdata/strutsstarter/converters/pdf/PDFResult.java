package org.nttdata.strutsstarter.converters.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionInvocation;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.result.ServletDispatcherResult;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;

public class PDFResult extends ServletDispatcherResult {

	@java.io.Serial
	private static final long serialVersionUID = 1L;

	public PDFResult() {
		super();
	}

	public PDFResult(String location) {
		super(location);
	}

	public void doExecute(String location, ActionInvocation invocation) {
		String actionName = invocation.getInvocationContext().getName();

		if (location.isEmpty())
			location = actionName;

		setLocation(location);

		HttpServletResponse response = ServletActionContext.getResponse();
		HttpServletRequest request = ServletActionContext.getRequest();

		try {
			Document document = new Document(PageSize.A4);
			ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
			PdfWriter pdfWriter = PdfWriter.getInstance(document, arrayOutputStream);
			document.open();

			PDFView pdfViewObj = (PDFView) Class.forName(getLocation()).getDeclaredConstructor().newInstance();
			pdfViewObj.buildPdfDocument(invocation, document, pdfWriter, request, response);

			document.close();

			response.setHeader("Content-Disposition", "attachment; filename=\"pets.pdf" );
			response.setContentType("application/pdf");
			response.setContentLengthLong(arrayOutputStream.size());
			ServletOutputStream outputStream = response.getOutputStream();
			arrayOutputStream.writeTo(outputStream);
			outputStream.flush();
			outputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
