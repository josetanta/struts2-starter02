package org.nttdata.strutsstarter.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableJpaRepositories(basePackages = "org.nttdata.strutsstarter.persistence.repository")
@EnableTransactionManagement
@RequiredArgsConstructor
public class AppConfig {

	private final DbConfig dbConfig;

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		var entityManager = new LocalContainerEntityManagerFactoryBean();
		entityManager.setDataSource(dataSource());
		entityManager.setPackagesToScan("org.nttdata.strutsstarter");

		HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setGenerateDdl(dbConfig.getGenerateDdl());
		jpaVendorAdapter.setShowSql(dbConfig.getShowSql());

		entityManager.setJpaVendorAdapter(jpaVendorAdapter);
		entityManager.setJpaProperties(additionalProps());

		return entityManager;
	}

	@Bean
	public DataSource dataSource() {
		var dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(dbConfig.getDriverClassName());
		dataSource.setUrl(dbConfig.getUrl());
		dataSource.setUsername(dbConfig.getUsername());
		dataSource.setPassword(dbConfig.getPassword());
		return dataSource;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		var transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor processor() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	private Properties additionalProps() {
		var properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", dbConfig.getHibernateDdlAuto());
		properties.setProperty("hibernate.dialect", dbConfig.getHibernateDialect());
		properties.setProperty("hibernate.format_sql", dbConfig.getFormatSql().toString());

		return properties;
	}
}
