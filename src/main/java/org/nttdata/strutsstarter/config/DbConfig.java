package org.nttdata.strutsstarter.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class DbConfig {
	@Value("${db.driver-class-name}")
	private String driverClassName;

	@Value("${db.url}")
	private String url;

	@Value("${db.username}")
	private String username;

	@Value("${db.password}")
	private String password;

	@Value("${hibernate.dialect}")
	private String hibernateDialect;

	@Value("${hibernate.ddl-auto}")
	private String hibernateDdlAuto;

	@Value("${hibernate.jpa.show-sql:false}")
	private Boolean showSql;

	@Value("${hibernate.jpa.generate-ddl:false}")
	private Boolean generateDdl;

	@Value("${hibernate.format_sql:false}")
	private Boolean formatSql;
}
