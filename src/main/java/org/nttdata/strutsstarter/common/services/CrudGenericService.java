package org.nttdata.strutsstarter.common.services;

import java.util.List;

public interface CrudGenericService<T, ID> {
	void create(T object);

	List<T> listObject();

	T singleObject(ID id);

	void update(T object, ID id);

	void delete(ID id);
}
