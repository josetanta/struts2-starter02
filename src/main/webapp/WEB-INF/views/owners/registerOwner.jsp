<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <s:head/>
    <title>Register owner</title>
</head>
<body>
<main>
    <h1><s:text name="owner.title" /></h1>
    <div>
        <s:form namespace="/owners" action="registerOwner" method="post" class="container p-4 bg-light">
            <s:textfield class="form-control" name="ownerBean.name" key="owner.form.name"/>
            <s:submit key="owner.form.submit" class="btn btn-primary"/>
        </s:form>
    </div>
    <div>
        <h3>Owners</h3>
        <ul class="list-group">
            <s:iterator var="owner" value="ownerList">
                <li class="list-group-item"><s:property value="id"/> - <s:property value="name"/></li>
            </s:iterator>
        </ul>
    </div>
</main>
</body>
</html>
