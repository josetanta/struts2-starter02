<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Details <s:property value="pet.name" />
</title>
</head>
<body>
	<div>
		<h1>
			Details
			<s:property value="pet.name" />
		</h1>
		<div class="card" style="width: 18rem;">
			<div class="card-body">
				<h5 class="card-title">
					<s:property value="pet.name" />
				</h5>
				<h6 class="card-subtitle mb-2 text-body-secondary">
					<s:property value="pet.codePet" />
				</h6>
				<div class="container d-flex flex-col gap-2">
					<s:url var="deletePet" action="deletePet" namespace="/pets">
						<s:param name="petId" value="%{pet.id}" />
					</s:url>
					<s:a href="%{deletePet}" class="btn btn-danger">Delete pet</s:a>
				</div>
			</div>
		</div>
	</div>
	<div>
		<s:form action="updatePet" namespace="/pets" class="shadow bg-light rounded-2 p-4">
			<s:hidden value="%{pet.id}" name="petId"/>
			<s:textfield name="pet.name" label="Name" class="form-control my-2" />
			<s:textfield name="pet.codePet" label="Code pet"
				class="form-control my-2" />
			<s:submit class="btn btn-secondary"/>
		</s:form>
	</div>
</body>
</html>
