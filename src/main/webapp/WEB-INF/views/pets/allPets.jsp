<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <s:head/>
    <title>All Pets</title>
</head>
<body>
<main>
    <h1><s:text name="pet.title"/></h1>
    <div>
        <s:if test="listOwnerId.size() > 0">
            <h4><s:text name="pet.sub-title" /></h4>
            <div>
                <s:form namespace="/pets" action="registerPet" method="post" class="shadow container p-4 bg-light rounded-2">
                    <s:textfield class="form-control" name="pet.name" key="pet.form.name"/>
                    <s:textfield class="form-control" name="pet.codePet" key="pet.form.code-pet"/>
                    <s:select
                            name="selectedID"
                            id="selectedID"
                            list="listOwnerId"
                            key="pet.form.select"
                            defaultValue="0"
                            class="form-select"
                    />
                    <s:textfield style="display: none;" id="text-select-id" name="selectedOwnerID"/>
                    <s:submit class="btn btn-primary" value="Register pet 🐶"/>
                </s:form>
                <br/>
            </div>
            <s:if test="petList.size() > 1">
            <div>
            	<div>
                	<s:url var="exportPdf" action="allPets" namespace="/pets">
                		<s:param name="exportTo" value="%{'pdf'}"/>
                	</s:url>
                	<s:a href="%{exportPdf}">PDF</s:a>
            	</div>
            	<br />
            	<div>
                	<s:url var="exportJson" action="allPets" namespace="/pets">
                		<s:param name="exportTo" value="%{'json'}"/>
                	</s:url>
                	<s:a href="%{exportJson}">JSON</s:a>
            	</div>
            </div>
            </s:if>
        </s:if>
        <s:else>
            <h4><s:text name="pet.message.empty"/> ☹</h4>
        </s:else>
    </div>
    <div>
        <ul>
            <s:iterator var="pet" value="petList">
                <li>
                    <s:url var="details" namespace="/pets" action="detailPet">
                        <s:param name="petId" value="%{id}"/>
                    </s:url>
                    <s:a href="%{details}">
                    <span>
                        <s:property value="name"/>
                    </span>
                    </s:a>
                    <br/>
                    <span>
                        <s:property value="codePet"/>
                    </span>
                    <br/>
                    <span>
                        Owner: <strong><s:property value="owner.name"/></strong>
                    </span>
                </li>
            </s:iterator>
        </ul>
    </div>
</main>
<script type="text/javascript">
    const selectedIDDOM = document.getElementById('selectedID');
    const txtInputIDDOM = document.getElementById('text-select-id');
    let value;

    if (value === undefined) {
        value = selectedIDDOM.value;
        txtInputIDDOM.value = value;
    }
    selectedIDDOM.addEventListener('change', (e) => {
        value = e.target.value;
        if (value !== 0)
            txtInputIDDOM.value = e.target.value;
    })
</script>
</body>
</html>
