<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.1 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page" %>

<head>
    <title>Index</title>
    <s:head/>
</head>
<body>
<div>
   
    <div>
        <s:form action="helloWorld" class="d-flex flex-col gap-2 bg-light p-4">
            <s:textfield class="form-control" key="index.form.name" name="name"/>
            <s:textfield class="form-control" key="index.form.date" name="dateNow"/>
            <s:submit class="btn btn-success" value="Send information"/>
        </s:form>
    </div>
</div>
</body>
</html>
	