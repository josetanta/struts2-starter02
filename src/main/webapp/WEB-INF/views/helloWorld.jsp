<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.1 Transitional//ES"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@taglib prefix="s" uri="/struts-tags" %>

<head>
    <title>Hello World</title>
    <s:head/>
</head>
<body>
<div>
    Hello <s:property value="name"/>, today is <s:property value="dateNow"/><br/>
    <s:text name="text.hello_world.title"/>
</div>
</body>
</html>
