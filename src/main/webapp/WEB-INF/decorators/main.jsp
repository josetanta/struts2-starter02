<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.1 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><decorator:title default="Struts Starter"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="<s:url value='/styles/main.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <decorator:head/>
</head>
<body id="page-home">
<div id="page">
    <div id="header" class="navbar bg-black">
        HEADER
        <hr/>
        
    </div>

    <div id="content" class="clearfix">
     	<div>
        	<s:url var="localeEN">
            	<s:param name="request_locale">en</s:param>
        	</s:url>
        	<s:url var="localeES">
            	<s:param name="request_locale">es</s:param>
        	</s:url>
        	<ul class="list-group">
            	<li class="list-group-item"><s:a class="btn" href="%{localeEN}">en</s:a></li>
            	<li class="list-group-item"><s:a class="btn" href="%{localeES}">es</s:a></li>
        	</ul>
        	<h4><s:text name="text.index.title"/></h4>
    	</div>
        <div id="main">
            <h3>Main Content</h3>
            <decorator:body/>
            <hr/>
        </div>
        <div id="sub">
            <h3>Sub Content</h3>
        </div>


        <div id="local">
            <h3>Local Nav. Bar</h3>
            <ul>
                <li><a href="<s:url namespace="/pets" action="allPets" />">Content of pets</a></li>
                <li><a href="<s:url namespace="/owners" action="ownerList" method="get" />">Content owner</a></li>
            </ul>
        </div>


        <div id="nav">
            <div class="wrapper">
                <h3>Nav. bar</h3>
                <ul class="clearfix">
                    <s:iterator var="ps" value="{'page01', 'page02', 'page03'}">
                        <li>
                            <a href="<s:url action="%{ps}" namespace="/pages" />">
                                <s:property/>
                            </a>
                        </li>
                    </s:iterator>
                    <li class="last"><a href="#">Menu 6</a></li>
                </ul>
            </div>
            <hr/>
        </div>
    </div>

    <div id="footer" class="clearfix">
        Footer
    </div>

</div>

<div id="extra1">&nbsp;</div>
<div id="extra2">&nbsp;</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>
